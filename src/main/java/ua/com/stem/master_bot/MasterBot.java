package ua.com.stem.master_bot;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import ua.com.stem.master_bot.handlers.Handler;

@Log4j2
@Component
public class MasterBot extends TelegramLongPollingBot {

    @Autowired
    private Handler mainHandler;
    @Value("${bot.token}")
    private String token;
    @Value("${bot.username}")
    private String username;


    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            mainHandler.handleUpdate(update);
        } catch (Exception e) {
            log.error(e);
        }
    }
}