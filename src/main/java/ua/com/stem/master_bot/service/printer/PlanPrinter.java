package ua.com.stem.master_bot.service.printer;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_FixedWidth;
import org.springframework.stereotype.Component;
import ua.com.stem.master_bot.model.Worker;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.lineSeparator;

@Component
public class PlanPrinter {

    public List<String> printWorkers(List<Worker> workers) {
        AsciiTable at = new AsciiTable();
        CWC_FixedWidth cwc = new CWC_FixedWidth();
        cwc.add(25);
        at.getRenderer().setCWC(cwc);
        at.addRule();
        List<String> result = new ArrayList<>();
        int j = 11;
        for (int i = 0; i < workers.size(); i++) {
            String workArea = workers.get(i).getWorkArea();
            String name = workers.get(i).getName().replace(" ", "");
            String workPlace = workers.get(i).getWorkPlace();
            String workShift = workers.get(i).getWorkShift();
            at.addRow("(" + workArea + ")" + name + "<br> <br>" + workPlace + "|" + workShift);
            at.addRule();
            if (i == j || i == workers.size() - 1) {
                result.add(at.render());
                at = new AsciiTable();
                at.addRule();
                at.getRenderer().setCWC(cwc);
                j += 11;
            }
        }
        return result;
    }

    public String printSummary(List<List<String>> summary) {
        String separator = "-".repeat(22) + lineSeparator();
        StringBuilder result = new StringBuilder(separator);
        for (List<String> strings : summary) {
            result.append(String.format("|" + "%-12s" + "|" + "%6s" + " |%s", strings.get(0), strings.get(1), lineSeparator()));
            result.append(separator);
        }
        return result.toString();
    }
}