package ua.com.stem.master_bot.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import ua.com.stem.master_bot.google_api.SheetsParser;
import ua.com.stem.master_bot.model.DayOfMonthPlan;
import ua.com.stem.master_bot.model.Worker;
import ua.com.stem.master_bot.service.cache.TelegramCache;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Log4j2
@Component
public class WorkerService {

    final
    SheetsParser sheetsParser;

    public WorkerService(SheetsParser sheetsParser) {
        this.sheetsParser = sheetsParser;
    }

    public Set<String> getWorkPlaces(List<Worker> workers) {
        Set<String> workPlaces = new HashSet<>();
        for (Worker worker : workers) {
            String workPlace;
            workPlace = worker.getWorkPlace();
            if (workPlace.equals("")) {
                workPlace = "Утеряно Р/М";
            }
            workPlaces.add(workPlace);
        }
        return workPlaces;
    }

    public List<Worker> getWorkersForDay(LocalDate date) throws GeneralSecurityException, IOException {
        List<List<Object>> sheet = getSheet(date);
        DayOfMonthPlan currentDay = DayOfMonthPlan.values()[(date.getDayOfMonth() - 1)];
        List<Worker> workers = new ArrayList<>();
        for (int i = 2; i < 60; i++) {
            List<Object> row = sheet.get(i);
            if (!row.isEmpty() && row.get(0) != null && (row.get(0).equals("П") || row.get(0).equals("Р") || row.get(0).equals("М") || row.get(0).equals("M2"))) {
                String workArea = (String) row.get(DayOfMonthPlan.getWorkArea());
                String name = (String) row.get(DayOfMonthPlan.getNAME());
                String workPlace = (String) row.get(currentDay.getWorkPlace());
                String workShift = (String) row.get(currentDay.getWorkShift());
                Worker worker = new Worker(workArea, name);
                worker.setWorkPlace(workPlace);
                worker.setWorkShift(workShift);
                if (name != null && !name.equals("")) {
                    workers.add(worker);
                }
            }
        }
        return workers;
    }

    public List<List<String>> getSummaryFromDay(LocalDate date) throws GeneralSecurityException, IOException {
        List<List<Object>> sheet = getSheet(date);
        DayOfMonthPlan currentDay = DayOfMonthPlan.values()[(date.getDayOfMonth() - 1)];
        List<List<String>> tableRows = new ArrayList<>();
        for (int i = 64; i < 84; i++) {
            List<Object> googleSheetsRow = sheet.get(i);
            String firstCell = (String) googleSheetsRow.get(currentDay.getWorkPlace());
            if (!firstCell.equals("")) {
                String secondCell = (String) googleSheetsRow.get(currentDay.getWorkShift());
                List<String> planRow = List.of(firstCell, secondCell.replace(" ", ""));
                tableRows.add(planRow);
            }
        }
        return tableRows;
    }

    public List<Worker> getWorkersWithWorkPlace(String workplace, String chatId) {
        List<Worker> workers = new ArrayList<>();
        if (workplace.equals("Утеряно Р/М")) {
            workplace = "";
        }
        for (Worker worker : TelegramCache.getWorkerList().get(chatId)) {
            if (worker.getWorkPlace().equals(workplace)) {
                workers.add(worker);
            }
        }
        return workers;
    }

    private List<List<Object>> getSheet(LocalDate date) throws GeneralSecurityException, IOException {
        List<List<Object>> sheet;
        try {
            sheet = sheetsParser.getSheetFromPlanTable(date);
        } catch (GeneralSecurityException | IOException e) {
            log.error(e);
            throw e;
        }
        return sheet;
    }
}