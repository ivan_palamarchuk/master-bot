package ua.com.stem.master_bot.service.printer;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_FixedWidth;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.lang.System.lineSeparator;

@Component
public class TablePrinter {

    public String printDayOffs(List<List<Object>> dayOffData) {
        AsciiTable at = new AsciiTable();
        CWC_FixedWidth cwc = new CWC_FixedWidth();
        cwc.add(25);
        at.getRenderer().setCWC(cwc);
        at.addRule();
        for (List<Object> tableLine : dayOffData) {
            at.addRow(tableLine.get(0) + "|" + tableLine.get(1) + lineSeparator());
            at.addRule();
        }
        return at.render();
    }
}
