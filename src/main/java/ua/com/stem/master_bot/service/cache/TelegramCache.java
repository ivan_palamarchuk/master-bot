package ua.com.stem.master_bot.service.cache;

import org.springframework.stereotype.Component;
import ua.com.stem.master_bot.model.Worker;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

@Component
public class TelegramCache {

    private static LocalDate dateCache;
    private static Map<String, List<Worker>> workerList = new WeakHashMap<>();
    private static String currentCase = "";
    private static String before = "";
    private static List<List<Object>> dayOffNames;
    private static List<List<Object>> dayOffs;

    private TelegramCache() {
    }

    public static List<List<Object>> getDayOffs() {
        return dayOffs;
    }

    public static void setDayOffs(List<List<Object>> dayOffs) {
        TelegramCache.dayOffs = dayOffs;
    }

    public static LocalDate getDateCache() {
        return dateCache;
    }

    public static void setDateCache(LocalDate dateCache) {
        TelegramCache.dateCache = dateCache;
    }

    public static Map<String, List<Worker>> getWorkerList() {
        return workerList;
    }

    public static String getCurrentCase() {
        return currentCase;
    }

    public static void setCurrentCase(String currentCase) {
        TelegramCache.currentCase = currentCase;
    }

    public static String getBefore() {
        return before;
    }

    public static void setBefore(String before) {
        TelegramCache.before = before;
    }

    public static List<List<Object>> getDayOffNames() {
        return dayOffNames;
    }

    public static void setDayOffNames(List<List<Object>> dayOffNames) {
        TelegramCache.dayOffNames = dayOffNames;
    }
}
