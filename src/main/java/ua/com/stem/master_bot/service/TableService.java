package ua.com.stem.master_bot.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import ua.com.stem.master_bot.service.cache.TelegramCache;

import java.util.ArrayList;
import java.util.List;

@Component
@Log4j2
public class TableService {

    public List<List<Object>> getDayOffsForThisMonthWithWorkArea(String workArea) {
        List<List<Object>> names = TelegramCache.getDayOffNames();
        List<List<Object>> dayOffs = TelegramCache.getDayOffs();
        List<List<Object>> result = new ArrayList<>();
        for (int i = 0; i < names.size(); i++) {
            List<Object> rowAreaAndName = names.get(i);
            List<Object> rowDayOffs = dayOffs.get(i);
            Object area = rowAreaAndName.get(0);
            if (area.equals(workArea) || workArea.equals("Полный список")) {
                Object name = rowAreaAndName.get(1);
                Object dayOff = rowDayOffs.get(0);
                List<Object> dataRow = List.of(name, dayOff);
                result.add(dataRow);
            }
        }
        return result;
    }
}