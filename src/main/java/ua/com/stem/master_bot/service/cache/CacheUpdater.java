package ua.com.stem.master_bot.service.cache;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.com.stem.master_bot.handlers.TableHandler;

import java.util.Timer;
import java.util.TimerTask;

@Component
@Log4j2
public class CacheUpdater extends TimerTask {

    @Autowired
    TableHandler tableHandler;
    private boolean isUpdateOn = false;

    @SneakyThrows
    @Override
    public void run() {
        try {
            log.info("Trying to set day off data to cache");
            tableHandler.setDayOffsForThisMonth();
        } catch (Exception e) {
            log.error("Exception occurred during setting day off data to cache");
            e.printStackTrace();
            run();
        }
    }

    public boolean isUpdateOn() {
        return isUpdateOn;
    }

    public void updateCache() {
        try {
            Timer timer = new Timer();
            timer.schedule(this, 0, 300000L);
            isUpdateOn = true;
        } catch (Exception e) {
            log.info(e);
            log.info("Cache updating already active");
            throw e;
        }
    }
}