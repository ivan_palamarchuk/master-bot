package ua.com.stem.master_bot.service;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Calendar {
    static final String CAL_EMPTY = "calEmpty";
    static final String CAL_DAY_MARK = "calDay";
    int year;
    int month;
    LocalDate dayOfMonth;
    boolean isComplete;
    InlineKeyboardMarkup markupKeyboard;

    public Calendar(int year, int month) {
        markupKeyboard = new InlineKeyboardMarkup();
        isComplete = false;
        this.year = year;
        this.month = month;
        dayOfMonth = LocalDate.of(year, month, 1);

        markupKeyboard.setKeyboard(formButtons());
    }

    static String getMonthName(int month) {
        String[] monthNames = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль",
                "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};
        return monthNames[month];
    }

    private List<List<InlineKeyboardButton>> formButtons() {
        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
        buttons.add(new ArrayList<>(Collections.singletonList(formYearButton())));
        buttons.add(new ArrayList<>(Collections.singletonList(formMonthButton())));
        buttons.add(formDaysOfWeekButton());
        //filling days
        for (int i = 0; i < 6; i++) {
            while (!isComplete) {
                buttons.add(getWeekButtons());
            }
        }
        buttons.add(formMonthChangeButton());
        return buttons;
    }

    private InlineKeyboardButton formYearButton() {
        InlineKeyboardButton yearButton = new InlineKeyboardButton();
        yearButton.setText(Integer.toString(year));
        yearButton.setCallbackData(CAL_EMPTY);
        return yearButton;
    }

    private InlineKeyboardButton formMonthButton() {
        InlineKeyboardButton monthButton = new InlineKeyboardButton();
        monthButton.setCallbackData(CAL_EMPTY);
        monthButton.setText(getMonthName(month - 1));
        return monthButton;
    }

    private List<InlineKeyboardButton> formDaysOfWeekButton() {
        List<InlineKeyboardButton> daysOfWeekCalendar = new ArrayList<>();
        String[] daysOfWeek = {"Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"};
        for (String s : daysOfWeek) {
            InlineKeyboardButton buttonMonth = new InlineKeyboardButton();
            buttonMonth.setText(s);
            buttonMonth.setCallbackData(CAL_EMPTY);
            daysOfWeekCalendar.add(buttonMonth);
        }
        return daysOfWeekCalendar;
    }

    private List<InlineKeyboardButton> formMonthChangeButton() {
        InlineKeyboardButton leftButton = new InlineKeyboardButton();
        InlineKeyboardButton rightButton = new InlineKeyboardButton();
        leftButton.setText("<");
        leftButton.setCallbackData(CAL_EMPTY + "..." + dayOfMonth.minusMonths(1).toString());
        rightButton.setText(">");
        rightButton.setCallbackData(CAL_EMPTY + "..." + dayOfMonth.plusMonths(1).toString());
        return new ArrayList<>(Arrays.asList(leftButton, rightButton));
    }

    private List<InlineKeyboardButton> getWeekButtons() {
        return new ArrayList<>(
                Arrays.asList(
                        getDayButton(DayOfWeek.MONDAY),
                        getDayButton(DayOfWeek.TUESDAY),
                        getDayButton(DayOfWeek.WEDNESDAY),
                        getDayButton(DayOfWeek.THURSDAY),
                        getDayButton(DayOfWeek.FRIDAY),
                        getDayButton(DayOfWeek.SATURDAY),
                        getDayButton(DayOfWeek.SUNDAY)
                ));
    }

    private InlineKeyboardButton getDayButton(DayOfWeek day) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        if (dayOfMonth.getDayOfWeek() == day && !isComplete) {
            button.setText(Integer.toString(dayOfMonth.getDayOfMonth()));
            button.setCallbackData(CAL_DAY_MARK + "..." + dayOfMonth);

            if (dayOfMonth.plusDays(1).getMonthValue() == dayOfMonth.getMonthValue()) {
                dayOfMonth = dayOfMonth.plusDays(1);
            } else {
                isComplete = true;
            }
        } else {
            button.setText(" ");
            button.setCallbackData(CAL_EMPTY);
        }
        return button;
    }

    public InlineKeyboardMarkup getCalendar() {
        return markupKeyboard;
    }
}