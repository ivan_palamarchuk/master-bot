package ua.com.stem.master_bot.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ua.com.stem.master_bot.MasterBot;

import java.util.List;

@Log4j2
@Component
public class MethodExecutor {

    @Autowired
    MasterBot bot;

    public void executeMessage(List<BotApiMethod<?>> messageList) throws TelegramApiException {
        for (BotApiMethod<?> method : messageList) {
            try {
                bot.execute(method);
            } catch (TelegramApiException e) {
                log.error(e);
                throw e;
            }
        }
    }
}
