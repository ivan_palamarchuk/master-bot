package ua.com.stem.master_bot.google_api;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.com.stem.master_bot.model.Month;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.util.List;

@Component
@Log4j2
public class SheetsParser {

    private final GoogleApiConfig googleApiConfig;
    String rangeName = "Отгулы!A5:B60";
    String rangeJan = "Отгулы!AM5:AM72";
    String rangeFeb = "Отгулы!BS5:BS72";
    String rangeMar = "Отгулы!DA5:DA72";
    String rangeApr = "Отгулы!EH5:EH72";
    String rangeMay = "Отгулы!FP5:FP72";
    String rangeJun = "Отгулы!GW5:GW72";
    String rangeJul = "Отгулы!IE5:IE72";
    String rangeAug = "Отгулы!JM5:JM72";
    String rangeSep = "Отгулы!KT5:KT72";
    String rangeOct = "Отгулы!MB5:MB72";
    String rangeNov = "Отгулы!NI5:NI72";
    String rangeDec = "Отгулы!OQ5:OQ72";


    @Value("${plan.spreadsheet.id}")
    private String planSpreadsheetId;
    @Value("${table.spreadsheet.id}")
    private String tableSpreadsheetId;


    public SheetsParser(GoogleApiConfig googleApiConfig) {
        this.googleApiConfig = googleApiConfig;
    }

    public List<List<Object>> getSheetFromPlanTable(LocalDate date) throws GeneralSecurityException, IOException {
        List<List<Object>> sheet;
        String spreadsheetId = planSpreadsheetId;
        String month = Month.values()[date.getMonthValue() - 1].toString();
        String range = month + "!A:BM";
        try {
            sheet = googleApiConfig.getSheetData(spreadsheetId, range);
        } catch (GeneralSecurityException | IOException e) {
            log.error(e);
            throw e;
        }
        return sheet;
    }

    public List<List<Object>> getDayOffSheetFromTable(String requiredRange) throws GeneralSecurityException, IOException {
        switch (requiredRange) {
            case "name":
                return getDayOffSheet(rangeName);
            case "JANUARY":
                return getDayOffSheet(rangeJan);
            case "FEBRUARY":
                return getDayOffSheet(rangeFeb);
            case "MARCH":
                return getDayOffSheet(rangeMar);
            case "APRIL":
                return getDayOffSheet(rangeApr);
            case "MAY":
                return getDayOffSheet(rangeMay);
            case "JUNE":
                return getDayOffSheet(rangeJun);
            case "JULY":
                return getDayOffSheet(rangeJul);
            case "AUGUST":
                return getDayOffSheet(rangeAug);
            case "SEPTEMBER":
                return getDayOffSheet(rangeSep);
            case "OCTOBER":
                return getDayOffSheet(rangeOct);
            case "NOVEMBER":
                return getDayOffSheet(rangeNov);
            case "DECEMBER":
                return getDayOffSheet(rangeDec);
        }
        log.error("Invalid range name" + requiredRange);
        throw new IllegalArgumentException("Invalid range name" + requiredRange);
    }

    private List<List<Object>> getDayOffSheet(String range) throws GeneralSecurityException, IOException {
        List<List<Object>> sheet;
        String spreadsheetId = tableSpreadsheetId;
        try {
            sheet = googleApiConfig.getSheetData(spreadsheetId, range);
        } catch (GeneralSecurityException | IOException e) {
            log.error(e);
            throw e;
        }
        return sheet;
    }
}