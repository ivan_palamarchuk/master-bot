package ua.com.stem.master_bot.handlers;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ua.com.stem.master_bot.service.MethodExecutor;
import ua.com.stem.master_bot.service.cache.CacheUpdater;
import ua.com.stem.master_bot.service.cache.TelegramCache;

import java.io.IOException;
import java.security.GeneralSecurityException;

@Log4j2
@Component
public class Handler {

    @Autowired
    MethodExecutor executor;
    @Autowired
    CalendarHandler calendarHandler;
    @Autowired
    PlanHandler planHandler;
    @Autowired
    MainMenuHandler mainMenuHandler;
    @Autowired
    DefaultMessageHandler defaultMessageHandler;
    @Autowired
    TableHandler tableHandler;
    @Autowired
    CacheUpdater cacheUpdater;

    public void handleUpdate(Update update) throws GeneralSecurityException, IOException, TelegramApiException {
        if (update.hasCallbackQuery()) {
            handleCallbackQueryUpdate(update);
        } else {
            handleMessageUpdate(update);
        }
    }

    private void handleCallbackQueryUpdate(Update update) throws TelegramApiException, GeneralSecurityException, IOException {
        String callbackData = update.getCallbackQuery().getData();
        log.info("Trying to handle update with keyboard with callback data: " + callbackData);
        if (callbackData.contains("calDay") && isPlanForDayCase()) {
            calendarHandler.handleCalendarDay(update);
            executor.executeMessage(planHandler.sendWorkPlaceList(update));
        } else if (callbackData.contains("calDay") && isSummaryCase()) {
            calendarHandler.handleCalendarDay(update);
            executor.executeMessage(planHandler.sendDaySummary(update));
        } else if (callbackData.contains("calEmpty")) {
            executor.executeMessage(calendarHandler.handleEmptyCalendarAnswer(update));
        } else if (callbackData.contains("workplace")) {
            executor.executeMessage(planHandler.getDayPlan(update));
        } else if (callbackData.contains("dayOff")) {
            executor.executeMessage(tableHandler.sendDayOffs(update));
        }
    }

    private void handleMessageUpdate(Update update) throws TelegramApiException {
        Message message = update.getMessage();
        if (message != null && (!message.getText().isEmpty() || message.getText() != null)) {
            log.info("Trying to handle update with message: " + message);
            switch (message.getText()) {
                case "Ежедневный план":
                    executor.executeMessage(planHandler.sendPlanMenu(message));
                    break;
                case "Назад":
                    executor.executeMessage(defaultMessageHandler.sendBack(message));
                    break;
                case "/start":
                case "/menu":
                    executor.executeMessage(mainMenuHandler.sendMainMenu(message));
                    break;
                case "План на день":
                    executor.executeMessage(calendarHandler.getDefaultCalendar(message.getChatId().toString()));
                    TelegramCache.setCurrentCase("plan");
                    break;
                case "Сводная инф-я за день":
                    executor.executeMessage(calendarHandler.getDefaultCalendar(message.getChatId().toString()));
                    TelegramCache.setCurrentCase("summary");
                    break;
                case "Please, choose the date":
                case "Выберите день":
                    break;
                case "Отгулы на сегодня":
                    executor.executeMessage(tableHandler.sendDayOffsButtons(update));
                    if (!cacheUpdater.isUpdateOn()) {
                        cacheUpdater.updateCache();
                    }
                    break;
                default:
                    executor.executeMessage(defaultMessageHandler.sendDefault(message));
            }
        }
    }

    private boolean isPlanForDayCase() {
        return TelegramCache.getCurrentCase().equals("plan");
    }

    private boolean isSummaryCase() {
        return TelegramCache.getCurrentCase().equals("summary");
    }
}