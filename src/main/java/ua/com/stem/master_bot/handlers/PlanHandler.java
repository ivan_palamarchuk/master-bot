package ua.com.stem.master_bot.handlers;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.com.stem.master_bot.model.Worker;
import ua.com.stem.master_bot.service.WorkerService;
import ua.com.stem.master_bot.service.cache.TelegramCache;
import ua.com.stem.master_bot.service.printer.PlanPrinter;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.lang.System.lineSeparator;

@Component
@Log4j2
public class PlanHandler {

    public static final String FULL_LIST = "Полный список";
    final
    WorkerService workerService;
    final
    PlanPrinter planPrinter;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");

    public PlanHandler(WorkerService workerService, PlanPrinter planPrinter) {
        this.workerService = workerService;
        this.planPrinter = planPrinter;
    }

    public List<BotApiMethod<?>> getDayPlan(Update update) {
        String workplace = update.getCallbackQuery().getData().split("\\.{3}")[1];
        String chatId = update.getCallbackQuery().getMessage().getChatId().toString();
        log.info("Sending day plan for: " + workplace);
        List<String> response;
        if (workplace.equals(FULL_LIST)) {
            response = planPrinter.printWorkers(TelegramCache.getWorkerList().get(chatId));
        } else {
            response = planPrinter.printWorkers(workerService.getWorkersWithWorkPlace(workplace,chatId));
        }
        List<BotApiMethod<?>> messageList = new ArrayList<>();
        while (!response.isEmpty()) {
            SendMessage message = new SendMessage();
            message.setChatId(chatId);
            message.setParseMode("html");
            message.setText("<b><i>" + workplace + "</i></b>" + lineSeparator() + "<pre>" + response.get(0) + "</pre>");
            messageList.add(message);
            response.remove(0);
        }
        return messageList;
    }

    public InlineKeyboardMarkup getWorkPlaceListButtons(Set<String> workPlaces) {
        log.info("Sending workPlace buttons");
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        InlineKeyboardButton buttonAll = new InlineKeyboardButton(FULL_LIST);
        buttonAll.setCallbackData("workplace..." + FULL_LIST);
        row.add(buttonAll);
        rowList.add(row);
        for (String workplace : workPlaces) {
            List<InlineKeyboardButton> rows = new ArrayList<>();
            InlineKeyboardButton button = new InlineKeyboardButton(workplace);
            button.setCallbackData("workplace..." + workplace);
            rows.add(button);
            rowList.add(rows);
        }
        markup.setKeyboard(rowList);
        return markup;
    }

    public List<BotApiMethod<?>> sendWorkPlaceList(Update update) throws GeneralSecurityException, IOException {
        List<Worker> workers;
        try {
            workers = workerService.getWorkersForDay(TelegramCache.getDateCache());
        } catch (GeneralSecurityException | IOException e) {
            log.error(e);
            throw e;
        }
        SendMessage sendMessage = new SendMessage();
        String chatId = update.getCallbackQuery().getMessage().getChatId().toString();
        sendMessage.setChatId(chatId);
        TelegramCache.getWorkerList().put(chatId, workers);
        sendMessage.setParseMode("html");
        sendMessage.setText("<b>" + formatter.format(TelegramCache.getDateCache()) + "</b>" + lineSeparator() + "Выберите место работы");
        sendMessage.setReplyMarkup(getWorkPlaceListButtons(workerService.getWorkPlaces(TelegramCache.getWorkerList().get(chatId))));
        return List.of(sendMessage);
    }

    public List<BotApiMethod<?>> sendPlanMenu(Message message) {
        log.info("Sending plan menu");
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setText("Меню ежедневного плана");
        TelegramCache.setBefore("/menu");
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row1 = new KeyboardRow();
        KeyboardRow row2 = new KeyboardRow();
        KeyboardRow row3 = new KeyboardRow();
        row1.add(new KeyboardButton("План на день"));
        row2.add(new KeyboardButton("Сводная инф-я за день"));
        row3.add(new KeyboardButton("Назад"));
        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);
        replyKeyboardMarkup.setKeyboard(keyboard);
        sendMessage.enableMarkdown(true);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        return List.of(sendMessage);
    }

    public List<BotApiMethod<?>> sendDaySummary(Update update) throws IOException, GeneralSecurityException {
        log.info("Sending day summary");
        String response;
        try {
            response = planPrinter.printSummary(workerService.getSummaryFromDay(TelegramCache.getDateCache()));
        } catch (GeneralSecurityException | IOException e) {
            log.error(e);
            throw e;
        }
        List<BotApiMethod<?>> messageList = new ArrayList<>();
        SendMessage message = new SendMessage();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId().toString());
        message.setParseMode("html");
        message.setText("<b>" + formatter.format(TelegramCache.getDateCache()) + "</b>" + lineSeparator() + "<pre>" + response + "</pre>");
        messageList.add(message);
        return messageList;
    }
}