package ua.com.stem.master_bot.handlers;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import ua.com.stem.master_bot.service.Calendar;
import ua.com.stem.master_bot.service.cache.TelegramCache;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
@Log4j2
public class CalendarHandler {

    public List<BotApiMethod<?>> getDefaultCalendar(String chatId) {
        log.info("Getting default calendar");
        InlineKeyboardMarkup cal = new Calendar(YearMonth.now().getYear(), YearMonth.now().getMonthValue()).getCalendar();
        SendMessage sendMessage = new SendMessage(chatId, "Выберите дату");
        sendMessage.setReplyMarkup(cal);
        return List.of(sendMessage);
    }

    public List<BotApiMethod<?>> handleEmptyCalendarAnswer(Update update) {
        String callback = update.getCallbackQuery().getData().split("\\.{3}")[1];
        log.info("Trying to handle calendar answer: " + callback);
        List<BotApiMethod<?>> messages = new ArrayList<>();
        String chatId = update.getCallbackQuery().getMessage().getChatId().toString();
        int msgId = update.getCallbackQuery().getMessage().getMessageId();
        if (Pattern.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}$", callback) && !update.getCallbackQuery().getData().isEmpty()) {
            //delete old calendar,3+
            DeleteMessage delMsg = new DeleteMessage();
            delMsg.setChatId(chatId);
            delMsg.setMessageId(msgId);
            messages.add(delMsg);

            //set new calendar
            LocalDate date = LocalDate.parse(callback);
            InlineKeyboardMarkup cal = new Calendar(date.getYear(), date.getMonthValue()).getCalendar();
            SendMessage sendMessage = new SendMessage(chatId, "Please, choose the date");
            sendMessage.setReplyMarkup(cal);
            messages.add(sendMessage);
        }
        return messages;
    }

    public void handleCalendarDay(Update update) {
        log.info("Trying to get date from calendar and set in in cache");
        LocalDate date = LocalDate.parse(update.getCallbackQuery().getData().split("\\.{3}")[1]);
        TelegramCache.setDateCache(date);
    }
}
