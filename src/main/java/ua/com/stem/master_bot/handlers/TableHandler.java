package ua.com.stem.master_bot.handlers;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ua.com.stem.master_bot.google_api.SheetsParser;
import ua.com.stem.master_bot.service.TableService;
import ua.com.stem.master_bot.service.cache.TelegramCache;
import ua.com.stem.master_bot.service.printer.TablePrinter;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@Component
@Log4j2
public class TableHandler {

    public static final String FULL_LIST = "Полный список";
    final
    TablePrinter tablePrinter;
    final
    TableService tableService;
    final
    SheetsParser sheetsParser;

    public TableHandler(TablePrinter tablePrinter, TableService tableService, SheetsParser sheetsParser) {
        this.tablePrinter = tablePrinter;
        this.tableService = tableService;
        this.sheetsParser = sheetsParser;
    }

    public List<BotApiMethod<?>> sendDayOffsButtons(Update update) {
        log.info("Sending day off buttons");
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId().toString());
        sendMessage.setParseMode("html");
        sendMessage.setText("Выберите участок");
        sendMessage.setReplyMarkup(getDayOffListButtons());
        return List.of(sendMessage);
    }

    private InlineKeyboardMarkup getDayOffListButtons() {
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        row.add(getDayOffButton(FULL_LIST));
        row.add(getDayOffButton("Производственный"));
        row2.add(getDayOffButton("Ремонтный"));
        row2.add(getDayOffButton("Монтажный 1"));
        row2.add(getDayOffButton("Монтажный 2"));
        rowList.add(row);
        rowList.add(row2);
        markup.setKeyboard(rowList);
        return markup;
    }

    private InlineKeyboardButton getDayOffButton(String buttonName) {
        InlineKeyboardButton button = new InlineKeyboardButton(buttonName);
        button.setCallbackData("dayOff..." + buttonName);
        return button;
    }

    public List<BotApiMethod<?>> sendDayOffs(Update update) {
        String response;
        String workArea = update.getCallbackQuery().getData().split("\\.{3}")[1];
        log.info("Sending day offs for: " + workArea);
        response = tablePrinter.printDayOffs(tableService.getDayOffsForThisMonthWithWorkArea(defineWorkArea(workArea)));
        List<BotApiMethod<?>> messageList = new ArrayList<>();
        SendMessage message = new SendMessage();
        message.setChatId(update.getCallbackQuery().getMessage().getChatId().toString());
        message.setParseMode("html");
        message.setText("<pre>" + response + "</pre>");
        messageList.add(message);
        return messageList;
    }

    private String defineWorkArea(String workAreaFromUpdate) {
        switch (workAreaFromUpdate) {
            case FULL_LIST:
                return FULL_LIST;
            case "Производственный":
                return "П";
            case "Ремонтный":
                return "Р";
            case "Монтажный 1":
                return "М";
            case "Монтажный 2":
                return "М2";
            default:
                log.error("Неверное название участка или функции: " + workAreaFromUpdate);
                return FULL_LIST;
        }
    }

    public void setDayOffsForThisMonth() throws GeneralSecurityException, IOException {
        List<List<Object>> dayOffNames = (sheetsParser.getDayOffSheetFromTable("name"));
        TelegramCache.setDayOffNames(dayOffNames);
        Month thisMonth = LocalDate.now().getMonth();
        List<List<Object>> dayOffs = sheetsParser.getDayOffSheetFromTable(thisMonth.toString());
        TelegramCache.setDayOffs(dayOffs);
    }
}