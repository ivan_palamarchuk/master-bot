package ua.com.stem.master_bot.handlers;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.List;

@Component
@Log4j2
public class DefaultMessageHandler {
    final
    MainMenuHandler mainMenuHandler;

    public DefaultMessageHandler(MainMenuHandler mainMenuHandler) {
        this.mainMenuHandler = mainMenuHandler;
    }

    List<BotApiMethod<?>> sendDefault(Message message) {
        log.info("Sending default message");
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText("Воспользуйтесь главным меню /menu");
        sendMessage.setChatId(message.getChatId().toString());
        return List.of(sendMessage);
    }

    List<BotApiMethod<?>> sendBack(Message message) {
        log.info("Return to previous menu");
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId().toString());
        return mainMenuHandler.sendMainMenu(message);
    }
}
