package ua.com.stem.master_bot.model;

import lombok.Data;

@Data
public class Worker {

    private String workPlace;
    private String workShift;
    private String workArea;
    private String name;

    public Worker(String workArea, String name) {
        this.workArea = workArea;
        this.name = name;
    }
}
