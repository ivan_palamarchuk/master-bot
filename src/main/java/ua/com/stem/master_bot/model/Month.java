package ua.com.stem.master_bot.model;

public enum Month {
    Янв,
    Фев,
    Мар,
    Апр,
    Май,
    Июнь,
    Июль,
    Авг,
    Сен,
    Окт,
    Ноя,
    Дек
}
