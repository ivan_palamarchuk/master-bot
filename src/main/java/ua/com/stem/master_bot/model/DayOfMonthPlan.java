package ua.com.stem.master_bot.model;

public enum DayOfMonthPlan {

    FIRST(2, 3),
    SECOND(4, 5),
    THIRD(6, 7),
    FOURTH(8, 9),
    FIFTH(10, 11),
    SIXTH(12, 13),
    SEVENTH(14, 15),
    EIGHTH(16, 17),
    NINTH(18, 19),
    TENTH(20, 21),
    ELEVENTH(22, 23),
    TWELFTH(24, 25),
    THIRTEENTH(26, 27),
    FOURTEENTH(28, 29),
    FIFTEENTH(30, 31),
    SIXTEENTH(32, 33),
    SEVENTEENTH(34, 35),
    EIGHTEENTH(36, 37),
    NINETEENTH(38, 39),
    TWENTIETH(40, 41),
    TWENTY_FIRST(42, 43),
    TWENTY_SECOND(44, 45),
    TWENTY_THIRD(46, 47),
    TWENTY_FOURTH(48, 49),
    TWENTY_FIFTH(50, 51),
    TWENTY_SIXTH(52, 53),
    TWENTY_SEVENTH(54, 55),
    TWENTY_EIGHTH(56, 57),
    TWENTY_NINTH(58, 59),
    THIRTIETH(60, 61),
    THIRTY_FIRST(62, 63);


    private static final int ROW = 3;
    private static final int NAME = 1;
    private static final int WORK_AREA = 0;
    private final int workPlace;
    private final int workShift;

    DayOfMonthPlan(int workPlace, int workShift) {
        this.workPlace = workPlace;
        this.workShift = workShift;
    }

    public static int getROW() {
        return ROW;
    }

    public static int getNAME() {
        return NAME;
    }

    public static int getWorkArea() {
        return WORK_AREA;
    }

    public int getWorkPlace() {
        return workPlace;
    }

    public int getWorkShift() {
        return workShift;
    }
}
