# Telegram bot-Interface for access to Human Resource management information

### **_Screenshots examples_**

![first sample](screenshots/Screenshot_1.png)
![second sample](screenshots/Screenshot_2.png)

### **_Table examples_**

![table sample1](screenshots/Table1.png)
![table sample2](screenshots/Table2.png)